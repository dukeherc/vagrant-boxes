#!/bin/bash
SOFTWARE=PYTHON
echo $SOFTWARE

##vagrant plugin install vagrant-vbguest
##vagrant plugin install vagrant-scp
vagrant destroy -f
vagrant up
case $SOFTWARE in
  "NGINX")
    SFW="nginx"
  ;;
  "PYTHON")
    SFW="python"
  ;;
  *)
    SFW="nginx"
  ;;
esac
echo "$SOFTWARE as $SFW"
sleep 10
ssh -i .vagrant/machines/default/virtualbox/private_key -o StrictHostKeyChecking=no -o UserKnownHostsFile=/dev/null vagrant@192.168.33.145 "sudo sh /vagrant/build_${SFW}.script"
vagrant scp :${SFW}_usrapp.tar.gz .
